<body id="page-top" data-spy="scroll" data-target=".navbar-custom">


  <div id="wrapper">

    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
      <div class="top-area">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-md-6">
              <p class="bold text-left">Lunes - Domingo, atención 8 am a 10 pm // Emergencias las 24 horas </p>
            </div>
            <div class="col-sm-6 col-md-6">
              <p class="bold text-right">Contacto +65315139</p>
            </div>
          </div>
        </div>
      </div>
      <div class="container navigation">

        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
          <a class="navbar-brand" href="index.html">
                    <img src="../img/logo.png" alt="" width="150" height="40" />LOGO
                </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#intro">Inicio</a></li>
            <li><a href="perfilUsuario.php">Mi Perfil</a></li>
            <li><a href="#service">Servico de Pacientes</a></li>
            <li><a href="#doctor">Instituciones</a></li>
            <li><a href="#facilities">Enfermedades y Tratamientos</a></li>
            <li><a href="#pricing">Noticias</a></li>
            <li><a href="#">Reserva</a></li>
            <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="badge custom-badge red pull-right">Extra</span>Contactos <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="index.html">Inicio</a></li>
                <li><a href="index-form.html">Registro</a></li>
                <li><a href="index-video.html">video</a></li>
              </ul>
            </li> -->
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container -->
    </nav>

