<?php
    require_once("../Model/Conexion.php");
    require_once("../Model/Usuario.php");

    $objUsuario = new Usuario();

        $listaUsuario = $objUsuario->listaUsuario();
        $listaRolUsuario = $objUsuario->listaRolUsuario();

    
    include "header_admin.php";
    include "body_admi.php";
?>
<!-- Section: intro -->
<section id="intro" class="intro">
    <div class="intro-content">
        <div class="container">
            <div class="row">
                <!-- IMPLEMENTACION LISTA PERSONAL  -->
                <table class="table table-bordered table-sm" id="busquedaTitulo">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Personal</td>
                            <td>CI</td>
                            <td>Telefono</td>
                            <td>Estado</td>
                            <td>Fecha de Registro</td>
                            <td>Rol Personal</td>
                            <!-- <td>Fotografia</td> -->
                    </thead>
                    <tbody>

                        <?php
                                        echo '<pre>';
                                        $contut=1;
                                        foreach($listaUsuario  as $listaUsuario ):  ?>
                        <tr>
                            <th scope="row"><?php echo($contut);?></th>
                            <td><?php echo $listaUsuario['Personal'] ?></td>
                            <td><?php echo $listaUsuario['ci'] ?></td>
                            <td><?php echo $listaUsuario['telefono'] ?></td>
                            <td><?php echo $listaUsuario['estado'] ?></td>
                            <td><?php echo $listaUsuario['registro'] ?></td>
                            <td><?php echo $listaUsuario['Rol'] ?></td>
                            <!-- <td><?php echo $listaUsuario['foto'] ?></td> -->


                            <!-- <td><a href="IUReporteFacultadCarrera.php?FacultadCarrera=<?php echo $listPersonal['idFacultad']; ?>"
                                                class="btn btn-dark">Ac</a></td>
                                        <td><a href="IUReporteFacultadAnio.php?FacultadAnio=<?php echo $listPersonal['idFacultad']; ?>"
                                                class="btn btn-dark">Tesis por año por carrera</a></td>
 -->



                        </tr>
                        <?php 
                                        $contut++;
                                        endforeach; ?>
                                         <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Registrar Nuevo Usuario
                        </button>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</section>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form autocomplete="off" method="post" action="../Controller/LNRegistrarUsuario.php"
                    enctype="multipart/form-data" class="needs-validation" novalidate>
                    <div class="container-flat-form">
                        <div class="title-flat-form title-flat-blue">Nuevo Personal</div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-sm-offset-2">

                                <div class="group-material">
                                    <span>Rol</span>
                                    <br>
                                    <select name="idTipoUsuario" id="idRol" class="tooltips-general material-control"
                                        data-toggle="tooltip" data-placement="top"
                                        title="Elige la Tipo de Tesis del libro">
                                        <option value="" disabled="" selected="">Selecciona un Tipo de Rol</option>
                                        <?php foreach($listaRolUsuario as $rol){ ?>
                                        <option value='<?php echo $rol['idTipoUsuario'];?>'><?php echo $rol['nombre'];?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </div>


                                <div class="group-material">
                                    <input name="ci" id="ci" type="text" class="tooltips-general material-control"
                                        placeholder="Escribe aquí tu ci" data-toggle="tooltip" data-placement="top"
                                        title="Escribe tu numero de ci, solamente números" pattern="[0-9]{7,8}">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label>CI</label>
                                </div>
                                <div class="group-material">
                                    <input name="primerNombre" id="primerNombre" type="text"
                                        class="tooltips-general material-control"
                                        placeholder="Escribe aquí tu primer Nombre" data-toggle="tooltip"
                                        data-placement="top" title="Escribe tu Apellido Paterno, solamente letras"
                                        pattern="[A-Za-z]{3,20}">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label>Primer Nombre</label>
                                </div>
                                <div class="group-material">
                                    <input name="segundoNombre" id="segundoNombre" type="text"
                                        class="tooltips-general material-control"
                                        placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                        data-placement="top" title="Escribe tu Segundo Nombre, solamente letras"
                                        pattern="[A-Za-z]{3,20}">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label>Segundo Nombre</label>
                                </div>
                                <div class="group-material">
                                    <input name="primerApellido" id="primerApellido" type="text"
                                        class="tooltips-general material-control"
                                        placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                        data-placement="top" title="Escribe tu Apellido Paterno, solamente letras"
                                        pattern="[A-Za-z]{3,20}">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label>Apellido Paterno</label>
                                </div>
                                <div class="group-material">
                                    <input name="segundoApellido" id="segundoApellido" type="text"
                                        class="tooltips-general material-control"
                                        placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                        data-placement="top" title="Escribe tu Apellido Materno, solamente letras"
                                        pattern="[A-Za-z]{3,20}">

                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label>Apellido Materno</label>
                                </div>
                                <div class="group-material">
                                    <input name="telefono" id="telefono" type="text"
                                        class="tooltips-general material-control" placeholder="Escribe aquí tu telefono"
                                        data-toggle="tooltip" data-placement="top"
                                        title="Escribe tu numero de telefono, solamente números" pattern="[0-9]{5,8}">

                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label>Telefono</label>
                                </div>
                                <div class="group-material">

                                    <input name="file" id="file" type="file" class="tooltips-general material-control">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label>Fotografia</label>
                                </div>
                                <div class="group-material">
                                    <input name="fechaRegistro" id="fechaRegistro" type="hidden"
                                        class="tooltips-general material-control" value="<?= $fechaActual?>">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>

                                </div>

                                <p class="text-center">
                                    <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i
                                            class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Limpiar</button>
                                    <button id="enviar" type="submit" class="btn btn-primary"><i
                                            class="zmdi zmdi-floppy"></i>
                                        &nbsp;&nbsp; Guardar</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>





<script src="../js/busqueda.js"></script>
<script src="../js/validacionFormulario.js"></script>



</body>

<?php
    include "footer.php";
?>