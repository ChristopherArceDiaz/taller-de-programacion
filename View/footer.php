<footer>

<div class="container">
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <!-- <div class="wow fadeInDown" data-wow-delay="0.1s">
        <div class="widget">
          <h5>About SystemDNS</h5>
          <p>
            Lorem ipsum dolor sit amet, ne nam purto nihil impetus, an facilisi accommodare sea
          </p>
        </div>
      </div> -->
      <div class="wow fadeInDown" data-wow-delay="0.1s">
        <div class="widget">
          <h5>Informacion</h5>
          <ul>
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Laboratorio</a></li>
            <li><a href="#">Tratamiento Medico</a></li>
            <li><a href="#">Terminos & Condiciones</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="wow fadeInDown" data-wow-delay="0.1s">
        <div class="widget">
          <h5>Centro SystemDNS</h5>
          <p>
            Nuestro centro de operaciones.
          </p>
          <ul>
            <li>
              <span class="fa-stack fa-lg">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
                          </span> Lunes - Domingo, atención 8 am a 10 pm // Emergencias las 24 horas
            </li>
            <li>
              <span class="fa-stack fa-lg">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
                          </span> +62 0888 904 711
            </li>
            <li>
              <span class="fa-stack fa-lg">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
                          </span> hello@SystemDNS.com
            </li>

          </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="wow fadeInDown" data-wow-delay="0.1s">
        <div class="widget">
          <h5>Nuestra Ubicación</h5>
          <p>The Suithouse V303, Kuningan City, Jakarta Indonesia 12940</p>

        </div>
      </div>
      <div class="wow fadeInDown" data-wow-delay="0.1s">
        <div class="widget">
          <h5>Nuestras Redes sociales</h5>
          <ul class="company-social">
            <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
            <li class="social-dribble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="sub-footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-6 col-lg-6">
        <div class="wow fadeInLeft" data-wow-delay="0.1s">
          <div class="text-left">
            <p>&copy; Copyright: tema ProOnliPc. Todos los derechos reservados.</p>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-6 col-lg-6">
        <div class="wow fadeInRight" data-wow-delay="0.1s">
          <div class="text-right">
            <div class="credits">
      
              <a href="http://www.servitecflhuaraz.com/">ProOnliPc Educacion Templates</a> by ProOnliPc
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</footer>