<?php
    include "header_doctor.php";
    include "body_doctor.php";
?>

    <!-- Section: intro -->
    <section id="intro" class="intro">
      <div class="intro-content">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
                <h2 class="h-ultra">Programa Medico</h2>
              </div>
              <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
                <h4 class="h-light">Brindarle atención médica de la mejor calidad</h4>
              </div>
              <div class="well well-trans">
                <div class="wow fadeInRight" data-wow-delay="0.1s">

                  <ul class="lead-list">
                    
                    <li><span class="fa fa-check fa-2x icon-success"></span> <span class="list"><strong>Los mejores doctores a tu servicio</strong><br />Contamos con profesionales altamente calificados para brindarte la mejor atención</span></li>
                    <li><span class="fa fa-check fa-2x icon-success"></span> <span class="list"><strong>Emergencias</strong><br />En caso de emergencias tenemos equipos adecuados para que actuen de la mejor manera posible</span></li>
                  </ul>
                  <p class="text-right wow bounceIn" data-wow-delay="0.4s">
                    <a href="#" class="btn btn-skin btn-lg">Más sobre nosotros <i class="fa fa-angle-right"></i></a>
                  </p>
                </div>
              </div>


            </div>
            <!-- <div class="col-lg-6">
              <div class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                <img src="../img/dummy/img-1.png" class="img-responsive" alt="" />
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </section>
