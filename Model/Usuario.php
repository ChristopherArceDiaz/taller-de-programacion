<?php

    class Usuario
    {
        public $idUsuario;
        // public $nombre;
        public $ci;
        public $primerNombre;
        public $segundoNombre;
        public $apellidoPaterno;
        public $apellidoMaterno;
        public $usuario;
        public $contrasenia;

        // public $nombreCompleto;

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        public function setIdUsuario($idUsuario){$this->idUsuario = $idUsuario;}
        // public function setNombre($nombre){$this->nombre = $nombre;}
        public function setCi($ci){$this->ci = $ci;}
        public function setPrimerNombre($primerNombre){$this->primerNombre = $primerNombre;}
        public function setSegundoNombre($segundoNombre){$this->segundoNombre = $segundoNombre;}
        public function setApellidoPaterno($apellidoPaterno){$this->apellidoPaterno = $apellidoPaterno;}
        public function setApellidoMaterno($apellidoMaterno){$this->apellidoMaterno = $apellidoMaterno;}
        public function setUsuario($usuario){$this->usuario = $usuario;}
        public function setContrasenia($contrasenia){$this->contrasenia = $contrasenia;}

        // public function setNombreCompleto($nombreCompleto){$this->nombreCompleto = $nombreCompleto;}



        public function getIdUsuario(){return $this->idUsuario;}
        // public function getNombre(){return $this->nombre;}
        public function getCi(){return $this->ci;}
        public function getPrimerNombre(){return $this->primerNombre;}
        public function getSegundoNombre(){return $this->segundoNombre;}
        public function getApellidoPaterno(){return $this->apellidoPaterno;}
        public function getApellidoMaterno(){return $this->apellidoMaterno;}
        public function getUsuario(){return $this->usuario;}
        public function getContrasenia(){return $this->contrasenia;}

        public function verificarPesonaContrasenia($usuario,$contrasenia)
        {

            $sqlVerificarPersona= "SELECT * FROM persona WHERE usuario = :usuario AND contrasenia = :contrasenia";

            $cmd = $this->conexion->prepare($sqlVerificarPersona);
            //asignando los valores de los parametros
            $cmd->bindParam(':usuario', $usuario);

            $cmd->bindParam(':contrasenia', $contrasenia);
            //ejecuta la consulta
            $cmd->execute();    


            $registroPersona = $cmd->fetch();
            if($registroPersona)
            {
                return $registroPersona;
            }
            else
            {
                return false;
            }
        }


        public function listaUsuario()
        {
 
            $sqlListaDePersonas = "SELECT CONCAT_WS(' ',p.primerApellido, p.segundoApellido, p.primerNombre, p.segundoNombre) AS Personal, p.ci AS ci, p.telefono AS telefono, IF(0<activo, 'ACTIVO', 'INACTIVO') AS estado, p.usuario AS usuario, p.fechaRegistro AS registro, p.fotografia AS foto, tp.nombre AS Rol
            FROM usuario p INNER JOIN tipoUsuario tp ON p.idTipoUsuario = tp.idTipoUsuario
            ORDER BY primerApellido;";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonas);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDePersonasDeLaConsulta = $cmd->fetchAll();
    
            return $listaDePersonasDeLaConsulta;
    
        }//end function

        public function listaRolUsuario()
        {

            $sqlListaDePersonas = "SELECT *
            FROM tipoUsuario
            ORDER BY nombre;";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonas);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            return $cmd->fetchAll();
    
        }//end function

   

        public function perfil($idPersona)
        {
 
            $sqlListaPersona = "SELECT CONCAT_WS(' ',primerApellido, segundoApellido,primerNombre, segundoNombre) AS Persona, ci AS ci, telefono AS telefono, IF(0<activo, 'ACTIVO', 'INACTIVO') AS estado, usuario AS usuario, fechaRegistro AS registro, fotografia AS foto
            FROM persona 
            WHERE idPersona = :idPersona
            ORDER BY primerApellido;";

            $cmd = $this->conexion->prepare($sqlListaPersona);
            //asignando los valores de los parametros


            // $encriptado = password_hash($contrasenia, PASSWORD_DEFAULT);
            // $cmd->bindParam(':contrasenia', $encriptado);

            $cmd->bindParam(':idPersona', $idPersona);
            //ejecuta la consulta
            $cmd->execute();    

            return $cmd->fetchAll();
    
            // return $listaDePersonasDeLaConsulta;
    
        }//end function


        public function informacionPersona($idPersona)
        {
            $sqlInformacionPersona = "SELECT * FROM persona p INNER JOIN rol r ON p.idRol = r.idRol WHERE idPersona = :idPersona";
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlInformacionPersona);

            $cmd->bindParam(':idPersona', $idPersona);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $informacionPersonaDeLaConsulta = $cmd->fetchAll();
    
            return $informacionPersonaDeLaConsulta;
    
        }//end function



        public function registrarUsuario($idTipoUsuario,$ci,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$telefono,$fotografia,$usuario,$contrasenia,$fechaRegistro) 
        {  
            $sqlInsertarUsuario = "INSERT INTO usuario(idTipoUsuario,ci,primerNombre,segundoNombre,primerApellido,segundoApellido,telefono,fotografia,usuario,contrasenia, fechaRegistro)
            VALUES (:idTipoUsuario,:ci,:primerNombre,:segundoNombre,:primerApellido,:segundoApellido,:telefono,:fotografia,:usuario,:contrasenia,:fechaRegistro)";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarUsuario);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idTipoUsuario', $idTipoUsuario);
                    $cmd->bindParam(':ci', $ci);
                    $cmd->bindParam(':primerNombre', $primerNombre);
                    $cmd->bindParam(':segundoNombre', $segundoNombre);
                    $cmd->bindParam(':primerApellido', $primerApellido);
                    $cmd->bindParam(':segundoApellido', $segundoApellido);
                    $cmd->bindParam(':telefono', $telefono);
                    $cmd->bindParam(':fotografia', $fotografia);
                    $cmd->bindParam(':usuario', $usuario);
                    $cmd->bindParam(':contrasenia', $contrasenia);           
                    $cmd->bindParam(':fechaRegistro', $fechaRegistro);
                

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        return 1;    
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function



    }

?>

