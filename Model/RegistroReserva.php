<?php
    class Usuario
        {
            public $idUsuario;
            // public $nombre;
            public $ci;
            public $primerNombre;
            public $segundoNombre;
            public $apellidoPaterno;
            public $apellidoMaterno;
            public $usuario;
            public $contrasenia;

            // public $nombreCompleto;

            function __Construct()
            {
                $this->conexion =  new Conexion();
            }

            public function setIdUsuario($idUsuario){$this->idUsuario = $idUsuario;}
            // public function setNombre($nombre){$this->nombre = $nombre;}
            public function setCi($ci){$this->ci = $ci;}
            public function setPrimerNombre($primerNombre){$this->primerNombre = $primerNombre;}
            public function setSegundoNombre($segundoNombre){$this->segundoNombre = $segundoNombre;}
            public function setApellidoPaterno($apellidoPaterno){$this->apellidoPaterno = $apellidoPaterno;}
            public function setApellidoMaterno($apellidoMaterno){$this->apellidoMaterno = $apellidoMaterno;}
            public function setUsuario($usuario){$this->usuario = $usuario;}
            public function setContrasenia($contrasenia){$this->contrasenia = $contrasenia;}

            // public function setNombreCompleto($nombreCompleto){$this->nombreCompleto = $nombreCompleto;}



            public function getIdUsuario(){return $this->idUsuario;}
            // public function getNombre(){return $this->nombre;}
            public function getCi(){return $this->ci;}
            public function getPrimerNombre(){return $this->primerNombre;}
            public function getSegundoNombre(){return $this->segundoNombre;}
            public function getApellidoPaterno(){return $this->apellidoPaterno;}
            public function getApellidoMaterno(){return $this->apellidoMaterno;}
            public function getUsuario(){return $this->usuario;}
            public function getContrasenia(){return $this->contrasenia;}
        }
?>