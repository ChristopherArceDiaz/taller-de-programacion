<?php

    class Personal
    {
        public $idPersonal;
        // public $nombre;
        public $ci;
        public $primerNombre;
        public $segundoNombre;
        public $apellidoPaterno;
        public $apellidoMaterno;
        public $usuario;
        public $contrasenia;

        // public $nombreCompleto;

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        public function setIdPersonal($idPersonal){$this->idPersonal = $idPersonal;}
        // public function setNombre($nombre){$this->nombre = $nombre;}
        public function setCi($ci){$this->ci = $ci;}
        public function setPrimerNombre($primerNombre){$this->primerNombre = $primerNombre;}
        public function setSegundoNombre($segundoNombre){$this->segundoNombre = $segundoNombre;}
        public function setApellidoPaterno($apellidoPaterno){$this->apellidoPaterno = $apellidoPaterno;}
        public function setApellidoMaterno($apellidoMaterno){$this->apellidoMaterno = $apellidoMaterno;}
        public function setUsuario($usuario){$this->usuario = $usuario;}
        public function setContrasenia($contrasenia){$this->contrasenia = $contrasenia;}

        // public function setNombreCompleto($nombreCompleto){$this->nombreCompleto = $nombreCompleto;}



        public function getIdPersonal(){return $this->idPersonal;}
        // public function getNombre(){return $this->nombre;}
        public function getCi(){return $this->ci;}
        public function getPrimerNombre(){return $this->primerNombre;}
        public function getSegundoNombre(){return $this->segundoNombre;}
        public function getApellidoPaterno(){return $this->apellidoPaterno;}
        public function getApellidoMaterno(){return $this->apellidoMaterno;}
        public function getUsuario(){return $this->usuario;}
        public function getContrasenia(){return $this->contrasenia;}

        // public function getNombreCompleto(){return $this->nombreCompleto;}


        //METODO SEGURO

        // public function verificarPesonaContrasenia($usuario,$contrasenia)
        // {
        //     // $clave = password_verify($contrasenia);
        //     // $sqlVerificarPersona= "SELECT * FROM persona WHERE usuario = :usuario AND contrasenia = :contrasenia";
        //     $sqlVerificarPersona= "SELECT * FROM persona WHERE usuario = :usuario AND contrasenia = :contrasenia";

        //     $cmd = $this->conexion->prepare($sqlVerificarPersona);
        //     //asignando los valores de los parametros
        //     $cmd->bindParam(':usuario', $usuario);

        //     // $encriptado = password_hash($contrasenia, PASSWORD_DEFAULT);
        //     // $cmd->bindParam(':contrasenia', $encriptado);

        //     $cmd->bindParam(':contrasenia', $contrasenia);
        //     //ejecuta la consulta
        //     $cmd->execute();    
            

        //     $registroPersona = $cmd->fetch();
        //     if($registroPersona)
        //     {
        //         return $registroPersona;
        //     }
        //     else
        //     {
        //         return false;
        //     }
        // }


        public function listaPersonal()
        {

            $sqlListaDePersonas = "SELECT CONCAT_WS(' ',p.primerApellido, p.segundoApellido, p.primerNombre, p.segundoNombre) AS Personal, p.ci AS ci, p.telefono AS telefono, IF(0<activo, 'ACTIVO', 'INACTIVO') AS estado, p.usuario AS usuario, p.fechaRegistro AS registro, p.fotografia AS foto, tp.nombre AS Rol
            FROM personal p INNER JOIN tipoPersonal tp ON p.idTipoPersonal = tp.idTipoPersonal
            ORDER BY p.primerApellido;";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonas);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            return $cmd->fetchAll();
    
        }//end function

        public function listaHospital()
        {

            $sqlListaDePersonas = "SELECT *
            FROM institucion
            ORDER BY nombre;";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonas);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            return $cmd->fetchAll();
    
        }//end function




        public function listaRolPersonal()
        {

            $sqlListaDePersonas = "SELECT *
            FROM tipoPersonal
            WHERE idTipoPersonal = 1 OR idTipoPersonal = 2 OR idTipoPersonal = 3
    
            ORDER BY nombre;";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonas);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            return $cmd->fetchAll();
    
        }//end function

    //     public function listaPersona()
    //     {

    //         $sqlListaPersona = "SELECT p.idPersona AS idPersona, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS Rol, p.fechaRegistro AS fechaRegistro
    //         FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
    //         ORDER BY p.primerApellido;";
            
    //         //preparando para ejecutar la consulta.
    //         $cmd = $this->conexion->prepare($sqlListaPersona);
    //         //ejecuta la consulta
    //         $cmd->execute();
    //         //variable para recibir la consulta en un areglo
    //         $listaDePersonaDeLaConsulta = $cmd->fetchAll();
    
    //         return $listaDePersonaDeLaConsulta;
    
    //     }//end function

    //     public function busquedaPersonal($busqueda)
    //     {

    //         $sqlBusquedaPersonal = "SELECT p.idPersona AS idPersona, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS Rol, p.telefono AS telefono, IF(0<activo, 'ACTIVO', 'INACTIVO') AS estado, p.usuario AS usuario
    //         FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
    //         WHERE CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) LIKE '%".$busqueda."%'
    //         OR p.primerApellido LIKE '%".$busqueda."%'
    //         OR p.segundoApellido LIKE '%".$busqueda."%'
    //         OR p.primerNombre LIKE '%".$busqueda."%'
    //         OR p.segundoNombre LIKE '%".$busqueda."%'
    //         OR p.ci LIKE '%".$busqueda."%'
    //         OR r.nombre LIKE '%".$busqueda."%'
    //         OR p.telefono LIKE '%".$busqueda."%'
    //         OR p.usuario LIKE '%".$busqueda."%'
    //         AND r.idRol = '3'
    //         ORDER BY dt.fechaHoraRegistro DESC";
    //         //preparando para ejecutar la consulta.
    //         $cmd = $this->conexion->prepare($sqlBusquedaPersonal);

    //       //   $cmd->bindParam(':busqueda', $busqueda);
    //         //ejecuta la consulta
    //         $cmd->execute();
    //         //variable para recibir la consulta en un areglo
    //         return $cmd->fetchAll();
    
    //       //   return $busquedaUsuarioAnioDeLaConsulta;
    
    //     }//end function

    //     public function perfil($idPersona)
    //     {
           

    //         $sqlListaPersona = "SELECT CONCAT_WS(' ',primerApellido, segundoApellido,primerNombre, segundoNombre) AS Persona, ci AS ci, telefono AS telefono, IF(0<activo, 'ACTIVO', 'INACTIVO') AS estado, usuario AS usuario, fechaRegistro AS registro, fotografia AS foto
    //         FROM persona 
    //         WHERE idPersona = :idPersona
    //         ORDER BY primerApellido;";

    //         $cmd = $this->conexion->prepare($sqlListaPersona);
    //         //asignando los valores de los parametros


    //         // $encriptado = password_hash($contrasenia, PASSWORD_DEFAULT);
    //         // $cmd->bindParam(':contrasenia', $encriptado);

    //         $cmd->bindParam(':idPersona', $idPersona);
    //         //ejecuta la consulta
    //         $cmd->execute();    

    //         return $cmd->fetchAll();
    
    //         // return $listaDePersonasDeLaConsulta;
    
    //     }//end function


    //     public function informacionPersona($idPersona)
    //     {
    //         $sqlInformacionPersona = "SELECT * FROM persona p INNER JOIN rol r ON p.idRol = r.idRol WHERE idPersona = :idPersona";
            
    //         //preparando para ejecutar la consulta.
    //         $cmd = $this->conexion->prepare($sqlInformacionPersona);

    //         $cmd->bindParam(':idPersona', $idPersona);
    //         //ejecuta la consulta
    //         $cmd->execute();
    //         //variable para recibir la consulta en un areglo
    //         $informacionPersonaDeLaConsulta = $cmd->fetchAll();
    
    //         return $informacionPersonaDeLaConsulta;
    
    //     }//end function


       public function registrarPersonal($idRol,$idInstitucion,$ci,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$telefono,$fotografia,$usuario,$contrasenia,$fechaRegistro) 
        {  
            $sqlInsertarPersona = "INSERT INTO persona(idRol,idInstitucion,ci,primerNombre,segundoNombre,primerApellido,segundoApellido,telefono,fotografia,usuario,contrasenia, fechaRegistro)
            VALUES (:idRol,:idInstitucion,:ci,:primerNombre,:segundoNombre,:primerApellido,:segundoApellido,:telefono,:fotografia,:usuario,:contrasenia,:fechaRegistro)";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarPersona);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idRol', $idRol);
                    $cmd->bindParam(':ci', $ci);
                    $cmd->bindParam(':primerNombre', $primerNombre);
                    $cmd->bindParam(':segundoNombre', $segundoNombre);
                    $cmd->bindParam(':primerApellido', $primerApellido);
                    $cmd->bindParam(':segundoApellido', $segundoApellido);
                    $cmd->bindParam(':telefono', $telefono);
                    $cmd->bindParam(':fotografia', $fotografia);
                    $cmd->bindParam(':usuario', $usuario);
                    $cmd->bindParam(':contrasenia', $contrasenia);           
                    $cmd->bindParam(':fechaRegistro', $fechaRegistro);
                    $cmd->bindParam(':idInstitucion', $idInstitucion);

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        return 1;    
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function

    //     public function registrarUsuario($idRol,$ci,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$telefono,$fotografia,$usuario,$contrasenia,$fechaRegistro,$fechaActualizacion) 
    //     {  
    //         $sqlInsertarUsuario = "INSERT INTO persona(idRol,ci,primerNombre,segundoNombre,primerApellido,segundoApellido,telefono,fotografia,usuario,contrasenia, fechaRegistro,fechaActualizacion)
    //         VALUES (:idRol,:ci,:primerNombre,:segundoNombre,:primerApellido,:segundoApellido,:telefono,:fotografia,:usuario,:contrasenia,:fechaRegistro,:fechaActualizacion)";
    //         try{
    //                 $cmd = $this->conexion->prepare($sqlInsertarUsuario);
    //                 //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
    //                 $cmd->bindParam(':idRol', $idRol);
    //                 $cmd->bindParam(':ci', $ci);
    //                 $cmd->bindParam(':primerNombre', $primerNombre);
    //                 $cmd->bindParam(':segundoNombre', $segundoNombre);
    //                 $cmd->bindParam(':primerApellido', $primerApellido);
    //                 $cmd->bindParam(':segundoApellido', $segundoApellido);
    //                 $cmd->bindParam(':telefono', $telefono);
    //                 $cmd->bindParam(':fotografia', $fotografia);
    //                 $cmd->bindParam(':usuario', $usuario);
    //                 $cmd->bindParam(':contrasenia', $contrasenia);           
    //                 $cmd->bindParam(':fechaRegistro', $fechaRegistro);
    //                 $cmd->bindParam(':fechaActualizacion', $fechaActualizacion);    

    //                 $cmd->execute();

    //                 $registroAfectado = $cmd->rowCount();
    //                 if($registroAfectado>0){
    //                     return 1;    
    //                 }else{
    //                     return 0;
    //                 }

    //         }catch(PDOException $e){
    //             echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
    //             exit();
    //             return 0;
    //         }
    //     }//end function


    //     public function actualizarPersona($idPersona,$idRol,$ci,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$telefono,$fechaActualizacion) //$idRol,
    //     {   
    //         //idRol = :idRol,
    //         $sqlActualizarTesis = "UPDATE persona SET  idRol=:idRol, ci = :ci, primerNombre = :primerNombre, segundoNombre = :segundoNombre, primerApellido = :primerApellido, segundoApellido = :segundoApellido, telefono = :telefono, fechaActualizacion = :fechaActualizacion  
    //         WHERE idPersona = :idPersona;";
    //         try{
    //                 $cmd = $this->conexion->prepare($sqlActualizarTesis);
    //                 //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
    //                 $cmd->bindParam(':idPersona', $idPersona);
                   
    //                 $cmd->bindParam(':idRol', $idRol);
    //                 $cmd->bindParam(':ci', $ci);
    //                 $cmd->bindParam(':primerNombre', $primerNombre);
    //                 $cmd->bindParam(':segundoNombre', $segundoNombre);
    //                 $cmd->bindParam(':primerApellido', $primerApellido);
    //                 $cmd->bindParam(':segundoApellido', $segundoApellido);
    //                 $cmd->bindParam(':telefono', $telefono);
    //                 $cmd->bindParam(':fechaActualizacion', $fechaActualizacion);
                   
    //                 // $cmd->bindParam(':fechaHoraRegistro', $fechaHoraRegistro);
    //                 // $cmd->bindParam(':idTipoTesis', $idTipoTesis);
    //                 $cmd->execute();

    //                 // $ActualizarConsulta = $cmd->fetchAll();
    
    //                 // return $ActualizarConsulta;
            
    //                 return 1;
    //                 $registroAfectado = $cmd->rowCount();
    //                  if($registroAfectado>0)
    //                 {
    //                     echo "ID ultimo: ".$this->conexion->lastInsertId();
    //                 }
    //                 else
    //                 {
    //                     return 0;
    //                 }

    //         }catch(PDOException $e){
    //             echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
    //             exit();
    //             return 0;
    //         }
    //     }//end function


    //     public function actualizarPerfil($idPersona,$ci,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$telefono,$fotografia,$fechaActualizacion) //$idRol,
    //     {   
    //         //idRol = :idRol,
    //         $sqlActualizarTesis = "UPDATE persona SET  ci = :ci, primerNombre = :primerNombre, segundoNombre = :segundoNombre, primerApellido = :primerApellido, segundoApellido = :segundoApellido, telefono = :telefono, fotografia= :fotografia, fechaActualizacion = :fechaActualizacion  
    //         WHERE idPersona = :idPersona;";
    //         try{
    //                 $cmd = $this->conexion->prepare($sqlActualizarTesis);
    //                 //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
    //                 $cmd->bindParam(':idPersona', $idPersona);
                   
    //                 // $cmd->bindParam(':idRol', $idRol);
    //                 $cmd->bindParam(':ci', $ci);
    //                 $cmd->bindParam(':primerNombre', $primerNombre);
    //                 $cmd->bindParam(':segundoNombre', $segundoNombre);
    //                 $cmd->bindParam(':primerApellido', $primerApellido);
    //                 $cmd->bindParam(':segundoApellido', $segundoApellido);
    //                 $cmd->bindParam(':telefono', $telefono);
    //                 $cmd->bindParam(':fotografia', $fotografia);
    //                 $cmd->bindParam(':fechaActualizacion', $fechaActualizacion);
                   
    //                 // $cmd->bindParam(':fechaHoraRegistro', $fechaHoraRegistro);
    //                 // $cmd->bindParam(':idTipoTesis', $idTipoTesis);
    //                 $cmd->execute();

    //                 // $ActualizarConsulta = $cmd->fetchAll();
    
    //                 // return $ActualizarConsulta;
            
    //                 return 1;
    //                 $registroAfectado = $cmd->rowCount();
    //                  if($registroAfectado>0)
    //                 {
    //                     echo "ID ultimo: ".$this->conexion->lastInsertId();
    //                 }
    //                 else
    //                 {
    //                     return 0;
    //                 }

    //         }catch(PDOException $e){
    //             echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
    //             exit();
    //             return 0;
    //         }
    //     }//end function


    //     public function inactivo($idPersona,$estado) //$idRol,
    //     {   
    //         //idRol = :idRol,
    //         $sqlActualizarTesis = "UPDATE persona SET activo = :estado  
    //         WHERE idPersona = :idPersona;";
    //         try{
    //                 $cmd = $this->conexion->prepare($sqlActualizarTesis);
    //                 //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
    //                 $cmd->bindParam(':idPersona', $idPersona);
                   
    //                 // $cmd->bindParam(':idRol', $idRol);
    //                 $cmd->bindParam(':estado', $estado);
                   
    //                 // $cmd->bindParam(':fechaHoraRegistro', $fechaHoraRegistro);
    //                 // $cmd->bindParam(':idTipoTesis', $idTipoTesis);
    //                 $cmd->execute();

    //                 // $ActualizarConsulta = $cmd->fetchAll();
    
    //                 // return $ActualizarConsulta;
            
    //                 return 1;
    //                 $registroAfectado = $cmd->rowCount();
    //                  if($registroAfectado>0)
    //                 {
    //                     echo "ID ultimo: ".$this->conexion->lastInsertId();
    //                 }
    //                 else
    //                 {
    //                     return 0;
    //                 }

    //         }catch(PDOException $e){
    //             echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
    //             exit();
    //             return 0;
    //         }
    //     }//end function




    }

?>

