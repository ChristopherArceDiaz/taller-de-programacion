    DROP DATABASE IF EXISTS hospital;
    CREATE DATABASE hospital;
    USE hospital

    -- TABLAS PRIMARIAS
    CREATE TABLE institucion(
    idInstitucion INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(30) NOT NULL,
    direccion VARCHAR(75),
    telefono VARCHAR(15)
    )ENGINE=InnoDB;

    CREATE TABLE tipoPersonal(
    idTipoPersonal INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR (15) NOT NULL
    )ENGINE=InnoDB;

    CREATE TABLE tipoUsuario(
    idTipoUsuario INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR (15) NOT NULL
    )ENGINE=InnoDB;

    CREATE TABLE areaEnfermedad(
    idAreaEnfermedad INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR (15) NOT NULL
    )ENGINE=InnoDB;

     CREATE TABLE especialidadPersonal(
    idEspecialidadPersonal INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR (15) NOT NULL
    )ENGINE=InnoDB;

    create table usuario (
    idUsuario INT AUTO_INCREMENT PRIMARY KEY,
    idTipoUsuario INT NOT NULL,
    ci INT (10) NOT NULL,
    primerNombre VARCHAR (15) NOT NULL,  
    segundoNombre VARCHAR (15),
    primerApellido VARCHAR (15) NOT NULL,
    segundoApellido VARCHAR (15),
    telefono INT (10) NOT NULL,
    fotografia VARCHAR (15),
    activo boolean NOT NULL DEFAULT 1,
    usuario VARCHAR (50) NOT NULL,
    contrasenia VARCHAR (50) NOT NULL,
    fechaRegistro DATE NOT NULL,
    FOREIGN KEY (idTipoUsuario) REFERENCES tipoUsuario(idTipoUsuario)
    )ENGINE=InnoDB;

    CREATE TABLE personal(
    idPersonalTesis INT AUTO_INCREMENT PRIMARY KEY,
    idInstitucion INT NOT NULL,
    idTipoPersonal INT NOT NULL, 
    ci INT (10) NOT NULL,
    primerNombre VARCHAR (15) NOT NULL,  
    segundoNombre VARCHAR (15),
    primerApellido VARCHAR (15) NOT NULL,
    segundoApellido VARCHAR (15),
    telefono INT (10) NOT NULL,
    fotografia VARCHAR (15),
    activo boolean NOT NULL DEFAULT 1,
    usuario VARCHAR (50) NOT NULL,
    contrasenia VARCHAR (50) NOT NULL,
    fechaRegistro DATE NOT NULL,
    FOREIGN KEY (idInstitucion) REFERENCES institucion(idInstitucion),
    FOREIGN KEY (idTipoPersonal) REFERENCES tipoPersonal(idTipoPersonal)
    )ENGINE=InnoDB; 


    CREATE TABLE enfermedad(
    idEnfermedad INT AUTO_INCREMENT PRIMARY KEY,
    idAreaEnfermedad INT NOT NULL,
    nombre VARCHAR (50) NOT NULL,
    resumen  LONGTEXT NOT NULL,
    descripcion  LONGTEXT NOT NULL,
    FOREIGN KEY(idAreaEnfermedad) REFERENCES areaEnfermedad(idAreaEnfermedad)
    )ENGINE=InnoDB; 

    CREATE TABLE reserva(
    idReserva INT AUTO_INCREMENT PRIMARY KEY,
    idEnfermedad INT NOT NULL,
    idUsuario INT NOT NULL,
    fechaRegistro DATE NOT NULL,
    fechaCita DATE NOT NULL,
    FOREIGN KEY(idEnfermedad) REFERENCES enfermedad(idEnfermedad),
    FOREIGN KEY(idUsuario) REFERENCES usuario(idUsuario)
    )ENGINE=InnoDB; 

    CREATE TABLE institucionUsuario(
    idInstitucion INT NOT NULL,
    idUsuario INT NOT NULL,
    FOREIGN KEY(idInstitucion) REFERENCES institucion(idInstitucion),
    FOREIGN KEY(idUsuario) REFERENCES usuario(idUsuario)
    )ENGINE=InnoDB;  




    -- OMITIDO POR LA TABLA PERSONAL Y TIPO-PERSONAL

    -- CREATE TABLE administrador(
    -- idAdministrador INT AUTO_INCREMENT PRIMARY KEY,
    -- idInstitucion INT NOT NULL,
    -- ci VARCHAR(10) UNIQUE NOT NULL,
    -- primerNombre VARCHAR(15)NOT NULL,
    -- segundoNombre VARCHAR(15),
    -- apellidoPaterno VARCHAR(15)NOT NULL,
    -- apellidoMaterno VARCHAR(15), 
    -- FOREIGN KEY (idInstitucion) REFERENCES institucion(idInstitucion)
    -- )ENGINE=InnoDB; 
    






































