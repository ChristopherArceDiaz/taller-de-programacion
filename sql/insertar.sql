    -- insertar tabla universidad  CARLOS OJEDA
    INSERT INTO institucion VALUES(NULL, "Hospital General de Vinto", "Av. Blanco Galindo",71737636);
    INSERT INTO institucion VALUES(NULL, "Hospital Esperanza", "Av. Simon I. Patiño",71737636);


    INSERT INTO tipopersonal VALUES(NULL,"Administrador");
    INSERT INTO tipopersonal VALUES(NULL,"Doctor");
    INSERT INTO tipopersonal VALUES(NULL,"Enfermero");
    INSERT INTO tipopersonal VALUES(NULL,"Usuario");

    INSERT INTO tipoUsuario VALUES(NULL,"Golden");
    INSERT INTO tipoUsuario VALUES(NULL,"Basico 1");
    INSERT INTO tipoUsuario VALUES(NULL,"Basico 2");

    INSERT INTO areaenfermedad VALUES(NULL,"Cardiología");
    INSERT INTO areaenfermedad VALUES(NULL,"Dermatología");
    INSERT INTO areaenfermedad VALUES(NULL,"Pediatría/Neonatología");
    INSERT INTO areaenfermedad VALUES(NULL,"Ginecología");
    INSERT INTO areaenfermedad VALUES(NULL,"Oftalmología");


    INSERT INTO areaenfermedad VALUES(NULL,"Medicina General");
    INSERT INTO areaenfermedad VALUES(NULL,"Cardiología");
    INSERT INTO areaenfermedad VALUES(NULL,"Dermatología");
    INSERT INTO areaenfermedad VALUES(NULL,"Pediatría/Neonatología");
    INSERT INTO areaenfermedad VALUES(NULL,"Ginecología");
    INSERT INTO areaenfermedad VALUES(NULL,"Oftalmología");



    INSERT INTO usuario VALUES(NULL,1,532673,'Ricardo','','Arjona','', 1789456,"../img/fotos/526.jpg",1,"rarjona","rarjona","2014-10-25");
    INSERT INTO usuario VALUES(NULL,1,387091,'Christian','','Nodal','', 2134679,"../img/fotos/526.jpg",1,"cnodal","cnodal","2010-04-25");

    INSERT INTO usuario VALUES(NULL,2,532673,'Lucia','','Molina','', 1789456,"../img/fotos/526.jpg",1,"lmolina","lmolina","2014-10-25");
    INSERT INTO usuario VALUES(NULL,2,387091,'Elvis','','Rodriguez','', 2134679,"../img/fotos/526.jpg",1,"rrodriguez","rrodriguez","2010-04-25");

    INSERT INTO usuario VALUES(NULL,3,532673,'Luis','','Jimenez','', 1789456,"../img/fotos/526.jpg",1,"ljimenez","ljimenez","2014-10-25");
    INSERT INTO usuario VALUES(NULL,3,387091,'Pedro','','Garcia','', 2134679,"../img/fotos/526.jpg",1,"pgarcia","pgarcia","2010-04-25");



    INSERT INTO personal VALUES(NULL,1,1,123456,'Karen','','Mamani','Zapata', 14512311,"../img/fotos/afdda3_e65a8cc388914a68943296bbf39d1c3b_mv2_d_2349_2832_s_2.jpg",1,"kmamani","kmamani","2014-10-25");
    INSERT INTO personal VALUES(NULL,1,1,234567,'Jhoana','','Arce','Flores', 22567822,"../img/fotos/carnet-olga-mas-peso.jpg",1,"jarce","jarce","2010-04-25");
    INSERT INTO personal VALUES(NULL,1,2,345678,'Ramiro','','Rodriguez','Quiroga', 33876533,"../img/fotos/526.jpg",1,"rrodriguez","rrodriguez","2005-11-25");

    INSERT INTO personal VALUES(NULL,1,2,456789,'Elvia','Elena','Perez','Zapata', 44894544,"../img/fotos/carnet-olga-mas-peso.jpg",1,"eperez","eperez","2014-10-25");
    INSERT INTO personal VALUES(NULL,1,2,567890,'Bianca','Alvina','Cusi','Lima', 55365855,"foto",1,"bcusi","bcusi","2010-04-25");
    INSERT INTO personal VALUES(NULL,1,3,678901,'Lucas','','Quiroga','Benitez', 66748966,"../img/fotos/526.jpg",1,"lquiroga","lquiroga","2005-11-25");
    INSERT INTO personal VALUES(NULL,1,3,789012,'Mari','Elena','Gutierrez','Poma', 73214777,"foto",1,"mgutierrez","mgutierrez","2014-10-25");
    INSERT INTO personal VALUES(NULL,1,3,890123,'Luciana','','Kachi','Mamani', 83621488,"../img/fotos/afdda3_e65a8cc388914a68943296bbf39d1c3b_mv2_d_2349_2832_s_2.jpg",1,"lkachi","lkachi","2010-04-25");

    INSERT INTO personal VALUES(NULL,2,1,901234,'Elvia','','Garcia','', 41207894,"../img/fotos/afdda3_e65a8cc388914a68943296bbf39d1c3b_mv2_d_2349_2832_s_2.jpg",1,"eperez","eperez","2014-10-25");
    INSERT INTO personal VALUES(NULL,2,2,132456,'Alvina','','Garcia','Orozco', 57102555,"../img/fotos/carnet-olga-mas-peso.jpg",1,"bcusi","bcusi","2010-04-25");
    INSERT INTO personal VALUES(NULL,2,2,243567,'Pedro','','Mendoza','Arias', 67901266,"../img/fotos/526.jpg",1,"lquiroga","lquiroga","2005-11-25");
    INSERT INTO personal VALUES(NULL,2,3,354678,'Liam','Elena','Zapana','Rodriguez', 77003477,"../img/fotos/carnet-olga-mas-peso.jpg",1,"mgutierrez","mgutierrez","2014-10-25");
    INSERT INTO personal VALUES(NULL,2,3,465789,'Luciana','','Kachi','Mamani', 85342388,"../img/fotos/images (1).jfif",1,"lkachi","lkachi","2010-04-25");


    INSERT INTO enfermedad VALUES(NULL,1,'Insuficiencia cardíaca','el corazón no es capaz de bombear la sangre suficiente y esta no llega en la cantidad necesaria al resto del organismo. Las enfermedades coronarias, la hipertensión o la diabetes son algunas de las causas de la insuficiencia cardíaca.','el corazón no es capaz de bombear la sangre suficiente y esta no llega en la cantidad necesaria al resto del organismo. Las enfermedades coronarias, la hipertensión o la diabetes son algunas de las causas de la insuficiencia cardíaca.');
    INSERT INTO enfermedad VALUES(NULL,1,'Arritmia','es otra de las enfermedades más comunes del corazón y consiste en un latido irregular del corazón. Este puede latir más lento de lo normal (bradicardia), más rápido de lo normal (taquicardia), o producir arritmias intermitentes que provocarán mareo, asfixia, palidez y sudoración entre otros síntomas.','es otra de las enfermedades más comunes del corazón y consiste en un latido irregular del corazón. Este puede latir más lento de lo normal (bradicardia), más rápido de lo normal (taquicardia), o producir arritmias intermitentes que provocarán mareo, asfixia, palidez y sudoración entre otros síntomas.');
    INSERT INTO enfermedad VALUES(NULL,1,'Aneurisma','se produce cuando las paredes arteriales se debilitan y se inflaman en alguna zona en concreto del cuerpo. Proteger nuestra salud de este problema es posible con hábitos de vida saludables y chequeos rutinarios, ya que muchas veces la inflamación no produce dolor ni se percibe por el paciente.','se produce cuando las paredes arteriales se debilitan y se inflaman en alguna zona en concreto del cuerpo. Proteger nuestra salud de este problema es posible con hábitos de vida saludables y chequeos rutinarios, ya que muchas veces la inflamación no produce dolor ni se percibe por el paciente.');
    INSERT INTO enfermedad VALUES(NULL,2,'Acne','El acné es una enfermedad de la piel común durante la adolescencia, aunque puede aparecer a cualquier edad. Consiste en la aparición de granos o puntos negros generalmente en la cara, pero también se pueden observar en el pecho, la espalda y los hombros.','Este trastorno se desarrolla cuando los folículos pilosos, la parte de la piel por donde nacen los cabellos, son obstruidos por grasa o células cutáneas muertas, lo que permite el crecimiento de bacterias.Puede causar angustia ya que afectan al aspecto, e incluso pueden dejar cicatrices. Existen tratamientos efectivos para curar este trastorno.');
    INSERT INTO enfermedad VALUES(NULL,2,'Cáncer de piel','El cáncer de piel suele desarrollarse en las zonas de la epidermis expuestas a la radiación solar, la cual provoca unas lesiones en las células que aumentan el riesgo de que estas se conviertan en células cancerosas. ','El cáncer de piel suele desarrollarse en las zonas de la epidermis expuestas a la radiación solar, la cual provoca unas lesiones en las células que aumentan el riesgo de que estas se conviertan en células cancerosas. Los síntomas suelen ser el desarrollo de úlceras, lesiones marrones, aparición de bultos, lunares sangrantes y zonas con picazón. El tratamiento oncológico dependerá de la zona en la que se haya desarrollado el tumor.');
    INSERT INTO enfermedad VALUES(NULL,3,'Varicela','Esta enfermedad está causada por el virus Varicela-Zoster, que pertenece a la familia de los herpes. Se caracteriza por la aparición de vesículas, que en un primer momento están llenas de un líquido claro pero después adquiere un color amarillento. Al cabo de unos días, las vesículas se rompen y se desarrolla una costra. Suelen aparecer en la cabeza y el tronco, y provocan prurito y en algunos casos fiebre alta.','Esta enfermedad está causada por el virus Varicela-Zoster, que pertenece a la familia de los herpes. Se caracteriza por la aparición de vesículas, que en un primer momento están llenas de un líquido claro pero después adquiere un color amarillento. Al cabo de unos días, las vesículas se rompen y se desarrolla una costra. Suelen aparecer en la cabeza y el tronco, y provocan prurito y en algunos casos fiebre alta.');
    INSERT INTO enfermedad VALUES(NULL,3,'Sarampión','Esta enfermedad está causada por un virus de la familia de los Paramixovirus y suele aparecer en la parte interior de la boca, las mejillas y el paladar. Más tarde, en cuestión de unos tres días, se extiende a la cara y el cuello. Entonces aparecen las típicas lesiones: pápulas rojas y levantadas que producen prurito. También puede provocar tos, fiebre y dolor muscular.','Esta enfermedad está causada por un virus de la familia de los Paramixovirus y suele aparecer en la parte interior de la boca, las mejillas y el paladar. Más tarde, en cuestión de unos tres días, se extiende a la cara y el cuello. Entonces aparecen las típicas lesiones: pápulas rojas y levantadas que producen prurito. También puede provocar tos, fiebre y dolor muscular.');
    INSERT INTO enfermedad VALUES(NULL,4,'Menopausia','cese definitivo de la menstruación secundario al cese de la actividad ovárica. Para confirmarla se precisan al  menos 12 meses de ausencia de menstruación (amenorrea).','cese definitivo de la menstruación secundario al cese de la actividad ovárica. Para confirmarla se precisan al  menos 12 meses de ausencia de menstruación (amenorrea).');
    INSERT INTO enfermedad VALUES(NULL,4,'Mioma uterino','Los miomas uterinos son la proliferación e incremento en forma de nódulos de las fibras musculares del útero. Estos nódulos de naturaleza benigna pueden ser únicos o múltiples, de diferentes tamaños y con diferentes localizaciones dentro del útero.','Los miomas uterinos son la proliferación e incremento en forma de nódulos de las fibras musculares del útero. Estos nódulos de naturaleza benigna pueden ser únicos o múltiples, de diferentes tamaños y con diferentes localizaciones dentro del útero.');
    INSERT INTO enfermedad VALUES(NULL,5,'Astigmatismo','es un trastorno en el que el ojo no enfoca la luz de forma pareja sobre la retina, el tejido sensible a la luz en la parte posterior del ojo. Esto puede hacer que las imágenes se vean borrosas o alargadas','es un trastorno en el que el ojo no enfoca la luz de forma pareja sobre la retina, el tejido sensible a la luz en la parte posterior del ojo. Esto puede hacer que las imágenes se vean borrosas o alargadas');
    INSERT INTO enfermedad VALUES(NULL,5,'Miopía ','es un trastorno en que los objetos cercanos se ven con claridad, mientras que los objetos lejanos se ven borrosos. Con la miopía, la luz se enfoca delante de la retina en vez de hacerlo sobre la retina','es un trastorno en que los objetos cercanos se ven con claridad, mientras que los objetos lejanos se ven borrosos. Con la miopía, la luz se enfoca delante de la retina en vez de hacerlo sobre la retina');
    